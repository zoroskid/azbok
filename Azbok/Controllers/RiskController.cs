﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Azbok.Models;
using System.Threading;
using Azbok.DAL;
using Microsoft.Ajax.Utilities;

namespace Azbok.Controllers
{
    public class RiskController : Controller
    {
        
        //
        // GET: /Risk/
        public ActionResult Risk(int? requestStatus)
        {
            if (requestStatus == null)
            {
                requestStatus = 3;
            }
            DalMethod dalMethod = new DalMethod();
            var creditRequestList = dalMethod.GetCreditRequests(requestStatus);
            switch (requestStatus)
            {
                case 3:
                    ViewBag.ID = "w";
                    break;
                case 1:
                    ViewBag.ID = "a";
                    break;
                case 2:
                    ViewBag.ID = "r";
                    break;
            }
            
            return View(creditRequestList);
        }


 
        public ActionResult OnWaiting()
        {

            return RedirectToAction("Risk", new {requestStatus = 3});

        }
         public ActionResult Approved()
        {
            return RedirectToAction("Risk", new { requestStatus = 1 });

         }
         public ActionResult Rejected()
        {
            return RedirectToAction("Risk", new { requestStatus = 2 });
        }
      
        public ActionResult ChangeRequestStatus(long creditRequestID, int requestStatus, int requestStatusOld)
        {
            var dal = new DalMethod();
            dal.ChangeCreditRequestStatus(creditRequestID, requestStatus);
            return RedirectToAction("Risk", new { requestStatus = requestStatusOld });
        }
    }
}