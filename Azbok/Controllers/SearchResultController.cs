﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Azbok.Models;

namespace Azbok.Controllers
{
    public class SearchResultController : Controller
    {
        //
        // GET: /SearchResult/
        public ActionResult SearchResult()
        {
      
                            
            return View();
        }

        [HttpPost]
        public ActionResult SearchResult(string searchCode, string searchName, string searchJmbg, string searchTip)
        {
            SearchData data = new SearchData();
            SearchData data2 = new SearchData();
            SearchData data3 = new SearchData();
            SearchData data4 = new SearchData();
            SearchData data5 = new SearchData();
            SearchData data6 = new SearchData();
            

            UsersApp lstdata = new UsersApp();

            data.SearchName = "Milan";
            data.SearchLastName = "Tulić";
            data.SearchType = "Keš kredit";
            data.SearchJmbg = 1508975710165;
            data.SearchCode = 12173;

            lstdata.usersall.Add( data);

            data2.SearchName = "Sanja";
            data2.SearchLastName = "Malić";
            data2.SearchType = "Keš kredit";
            data2.SearchJmbg = 2309987715152;
            data2.SearchCode = 12658;

            lstdata.usersall.Add(data2);

            data3.SearchName = "Nikola";
            data3.SearchLastName = "Peković";
            data3.SearchType = "Keš kredit";
            data3.SearchJmbg = 1106980710154;
            data3.SearchCode = 45879;

            lstdata.usersall.Add(data3);

            data4.SearchName = "Milan";
            data4.SearchLastName = "Tulić";
            data4.SearchType = "Keš kredit";
            data4.SearchJmbg = 1508975710165;
            data4.SearchCode = 57849;
            
            lstdata.usersall.Add(data4);
            
            data5.SearchName = "Petar";
            data5.SearchLastName = "Kuzmanović";
            data5.SearchType = "Keš kredit";
            data5.SearchJmbg = 16119730254;
            data5.SearchCode = 65987;

            lstdata.usersall.Add(data5);

            data6.SearchName = "Maša";
            data6.SearchLastName = "Lajović";
            data6.SearchType = "Keš kredit";
            data6.SearchJmbg = 1611973715128;
            data6.SearchCode = 548796;

            lstdata.usersall.Add(data6);
            ViewData["Users"] = lstdata;
            return View(lstdata);
        }


        public ActionResult Generate()
        {
            UserData u = new UserData();

            u.brojLicneKarte = "006445871";
            u.drzavljanstvo = "Srpsko";
            u.email = "milan.tulic@cmdsolutions.com";
            u.fiksni = "011/311-90-94";
            u.godineStaza = "10";
            u.ime = "Milan";
            u.imeOca = "Dragan";
            u.iznos = "5.000,00";
            u.jmbg = "1508975710165";
            u.kompanijaIme = "CmdSolutions";
            u.kompanijaPozicija = "Programer";
            u.kompanijaWeb = "www.cmdsolutions.co.rs";
            u.mesecnaPrimanja = "120.000,00";
            u.mobilni = "063/128-17-10";
            u.obustavljeniIznos = "0";
            u.Pib = "123654";
            u.pol = "Muški";
            u.prezime = "Tulić";
            u.ProcenatUcesca = "15";
            u.rodjenjeDan = "15";
            u.rodjenjeDrzava = "Srbija";
            u.rodjenjeGodina = "1975";
            u.rodjenjeGrad = "Beograd";
            u.rodjenjeMesec = "08";
            u.rodjenjeOpstina = "Čukarica";
            u.rokOtplateMeseci = "12";
            u.stanovanjeDan = "01";
            u.stanovanjeGodina = "2000";
            u.stanovanjeGrad = "Beograd";
            u.stanovanjeMesec = "06";
            u.stanovanjeOpstina = "Čukarica";
            u.stanovanjeUlica = "Olimpijskih Brigada 32";
            u.stecenoZvanje = "VSS";
            u.trazeniIznos = "500000";
            u.vrstaSvojine = "Privatno";
            u.zakupninaIznos = "0";
            u.zaposlenjeStatus = "Stalno";
            u.kreditValuta = "EUR";
            u.rokOtplateMeseci = "12";
            u.ProcenatUcesca = "20";
            u.tip = "Keš";
 
            return RedirectToAction("Index", "Home", u);
            
        }


  
	}
}