﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Azbok.DAL;
using Azbok.Models;
using System.Threading;

namespace Azbok.Controllers
{
    public class HomeController : Controller
    {

        DAL.DalMethod dalmethod = new DalMethod();
        public ActionResult Index()
        {        
            UserData u = new UserData();

       
            ViewBag.ComboBox = ControlsType.Control.ComboBox;
            ViewBag.Label = ControlsType.Control.Label;
            ViewBag.RadioButton = ControlsType.Control.RadioButton;


            List<string> TipKredita = new List<string>();

            TipKredita.Add("Keš kredit");
            TipKredita.Add("Stambeni kredit");
            TipKredita.Add("Potrošački kredit");
            TipKredita.Add("Kredit za penzionere");
            
            ViewBag.ListOfClientIDs = new SelectList(TipKredita);

            List<string> Valuata = new List<string>();

            Valuata.Add("EUR");
            Valuata.Add("RSD");


            ViewBag.Valuta = new SelectList(Valuata);

            var obj = PopulateParametars();



            return View(obj);
        }

        public ModelParamList PopulateParametars()
        {
            DalMethod Method = new DalMethod();
          
            var ParametarsList = Method.ReadParametarField();
            ModelParamList paramlist = new ModelParamList();
            paramlist.ObjectItems2.Clear();
            int num = 0;
            foreach (ModelParam p in ParametarsList)
            {
               
                ModelParam list2 = new ModelParam()
                {
                    Category = p.Category,
                    ControlTypeString = p.ControlTypeString,
                    DescriptionLabel = p.DescriptionLabel,
                    IsDefault = p.IsDefault,
                    Icon = p.Icon,
                    Id = p.Id,
                    Name = p.Name,
                    Placeholder = p.Placeholder,
                    OrderIndex = p.OrderIndex

                };

                
                if (p.ControlTypeString == "ComboBox")
                {
                   
                    list2.indexOfCmb = num;
                    list2.DropdownsList.Add(GenerateSelectList(p.Id, p.Name));
                    num++;
                }
                paramlist.ObjectItems2.Add(list2);
            }
                 
            return paramlist;

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }






        private SelectList GenerateSelectList(long Id , string Name)
        {
            //Create a list of select list items - this will be returned as select list
            List<SelectListItem> newList = new List<SelectListItem>();
            
            foreach (KeyValuePair<int, string> entry in dalmethod.GenerateComboBox(Id))
            {
                SelectListItem selListItem = new SelectListItem()
                {
                    Text = entry.Value,
                    Value = entry.Key.ToString() + "_" + Name,
                    Selected = false
                };

                //Add select list item to list of selectlistitems
                newList.Add(selListItem);

            }

            //Return the list of selectlistitems as a selectlist
            return new SelectList(newList, "Value", "Text", null);

        }


        [HttpPost]
        public ActionResult AddAnswer(ModelParamList ModelParam)
        {

           return  RedirectToAction("Index");
        }



    }
}