﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Azbok.Models;
using System.Threading;
using Microsoft.Vbe.Interop;
using Excel = Microsoft.Office.Interop.Excel;
using Azbok.DAL;
using ExporterObjects;

namespace Azbok.Controllers
{
    public class ResultController : Controller
    {
        //
        // GET: /Result/
        public ActionResult Result(CreditRequest c)
        {
            DAL.DalMethod dal = new DalMethod();

            ViewBag.IsLoaded = false;
            var lista = dal.CheckFlag(c);

 
                ViewBag.IsLoaded = true;
            

            return View();
        }

        public ActionResult EntityToExcelSheet(string fileName)
        {
            DataTable dt = DataTableGenerator();

            string excelFilePath = "D:\\OtplatniPlan\\PlanOtplate.xls";
            string sheetName = "Otplatni plan";

            Excel.Application oXL;
            Excel.Workbook oWB;
            Excel.Worksheet oSheet;
            Excel.Range oRange;
            try
            {
                // Start Excel and get Application object.
                oXL = new Excel.Application();

                // Set some properties
                oXL.Visible = true;
                oXL.DisplayAlerts = false;

                // Get a new workbook. 
                oWB = oXL.Workbooks.Add(System.Reflection.Missing.Value);
                oWB = oXL.Workbooks.Add();

                // Get the active sheet 
                oSheet = (Excel.Worksheet)oWB.ActiveSheet;
                oSheet.Name = sheetName;

                // Process the DataTable
                // BE SURE TO CHANGE THIS LINE TO USE *YOUR* DATATABLE 
                //DataTable dt = EntityToDataTable(result, ctx);
                int rowCount = 1;



                foreach (DataRow dr in dt.Rows)
                {


                    rowCount += 1;
                    for (int i = 1; i < dt.Columns.Count + 1; i++)
                    {
                        // Add the header the first time through 
                        if (rowCount == 2)
                            oSheet.Cells[1, i] = dt.Columns[i - 1].ColumnName;
                        oSheet.Cells[rowCount, i] = dr[i - 1].ToString();
                    }
                }


                // Resize the columns 
                oRange = oSheet.Range[oSheet.Cells[1, 1], oSheet.Cells[rowCount, dt.Columns.Count]];
                oRange.Columns.AutoFit();



                // Save the sheet and close 
                oSheet = null;
                oRange = null;
                oWB.SaveAs(excelFilePath, Excel.XlFileFormat.xlWorkbookNormal, System.Reflection.Missing.Value,
                    System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value,
                    Excel.XlSaveAsAccessMode.xlExclusive, System.Reflection.Missing.Value,
                    System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value);

                oWB = null;

            }
            catch (Exception ex)
            {

            }
            return View("Result");
        }

        public ActionResult ExportXml()
        {
            List<CreditResultData> list = CreditResultData.GetData();
            ExportList<CreditResultData> exp = new ExportList<CreditResultData>();
            exp.PathTemplateFolder = Server.MapPath("~/ExportTemplates/CreditResultData");
            string filePathExport = Server.MapPath("~/ExportTemplates/CreditResultData/KreditXml" + ExportBase.GetFileExtension((ExportToFormat)ExportToFormat.XML));
            exp.ExportTo(list, (ExportToFormat)ExportToFormat.XML, filePathExport);
            return this.File(filePathExport, "application/octet-stream", System.IO.Path.GetFileName(filePathExport));          
        }

        public ActionResult ExportPDF()
        {
            
            List<CreditResultData> list = CreditResultData.GetData();
            ExportList<CreditResultData> exp = new ExportList<CreditResultData>();
            exp.PathTemplateFolder = Server.MapPath("~/ExportTemplates/CreditResultData");
            string filePathExport = Server.MapPath("~/ExportTemplates/CreditResultData/KreditPdf" + ExportBase.GetFileExtension((ExportToFormat)ExportToFormat.PDFtextSharpXML));
            
            exp.ExportTo(list, (ExportToFormat)ExportToFormat.PDFtextSharpXML, filePathExport);
            return this.File(filePathExport, "application/octet-stream", System.IO.Path.GetFileName(filePathExport));
        }

        public ActionResult ExportWord()
        {
            List<CreditResultData> list = CreditResultData.GetData();
            ExportList<CreditResultData> exp = new ExportList<CreditResultData>();
            exp.PathTemplateFolder = Server.MapPath("~/ExportTemplates/CreditResultData");
            string filePathExport = Server.MapPath("~/ExportTemplates/CreditResultData/KreditWord" + ExportBase.GetFileExtension((ExportToFormat)ExportToFormat.Word2003XML));
            exp.ExportTo(list, (ExportToFormat)ExportToFormat.Word2007, filePathExport);
            return this.File(filePathExport, "application/octet-stream", System.IO.Path.GetFileName(filePathExport));
        }

        private DataTable DataTableGenerator()
        {
            DataTable dtName = new DataTable();
            dtName.Clear();
            dtName.Columns.Add("Rata");
            dtName.Columns.Add("Isplata kredita");
            dtName.Columns.Add("Anuitet");
            dtName.Columns.Add("Otplata");
            dtName.Columns.Add("Uplata kamate");
            dtName.Columns.Add("Učešće");
            dtName.Columns.Add("Stanje kredita");

            dtName.Rows.Add(new object[] { "0", "5.000,00", "","","","1.000,00", "4.000,00" });
            dtName.Rows.Add(new object[] { "1", "", "359,15", "312,48", "46,67", "", "3.687,52" });
            dtName.Rows.Add(new object[] { "2", "", "359,15", "316,13", "43,02", "", "3.371,39" });
            dtName.Rows.Add(new object[] { "3", "", "359,15", "319,82", "39,33", "", "3.051,58" });
            dtName.Rows.Add(new object[] { "4", "", "359,15", "323,55", "35,60", "", "2.728,03" });
            dtName.Rows.Add(new object[] { "5", "", "359,15", "327,32", "31,83", "", "2.400,71" });
            dtName.Rows.Add(new object[] { "6", "", "359,15", "331,14", "28,01", "", "2.069,57" });
            dtName.Rows.Add(new object[] { "7", "", "359,15", "335,00", "24,14", "", "1.734,56" });
            dtName.Rows.Add(new object[] { "8", "", "359,15", "338,91", "20,24", "", "1.395,65" });
            dtName.Rows.Add(new object[] { "9", "", "359,15", "342,87", "16,28", "", "1.052,79" });
            dtName.Rows.Add(new object[] { "10", "", "359,15", "346,87", "12,28", "", "705,92" });
            dtName.Rows.Add(new object[] { "11", "", "359,15", "350,91", "8,24", "", "355,01" });
            dtName.Rows.Add(new object[] { "12", "", "359,15", "355,01", "4,14", "", "0,00" });
            

        
            return dtName;
        }
    }
}