﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Azbok.Models;
using Azbok.DAL;
using System.IO;

namespace Azbok.Controllers
{
    public class AddParametarController : Controller
    {

        DAL.DalMethod dalmethod = new DalMethod();


        public ActionResult AddParametar(ModelForAdmin model, string[][] data)
        {
            
            
            
            ItemName GetItem = new ItemName();

            ViewBag.IsPersonal = TempData["message"];
            GenerateViewBag(model, GetItem);

            if (model.ValuesId != null)
            {
                GenerateParametarType(model,GetItem);
            }

            if(model.Parametar != null)
            {
                if (model.Parametar.Name != null)
                {
                    AddParametarSwitch(model);
                }
            }

            ModelForAdmin Model = new ModelForAdmin();
         
            model.ModelListParamModel.ObjectItems2.Clear();
            Model.DropdownsList.Clear();

            Model = PupulateModels(model);


            if (GetItem.Name == "Models")
            {
                Model.ActiveModel = GetItem.Id;
            }

            Model.ValueId = model.ValueId;
            return View(Model);
        }


        [HttpPost]

        public ActionResult tableData(Object data)
        {

            return View();

        }

        private void GenerateViewBag(ModelForAdmin model, ItemName GetItem)
        {
            if (model.Parametar != null)
            {
                if (model.Parametar.NoInterval != null)
                {
                    ViewBag.NoIntervals = model.Parametar.NoInterval;
                }
            }
            if (model.ValuesId != null && model.ValuesId[0] != "System.String[]")
            {

                GetItem.GenerateItems(model.ValuesId[0]);
                
                ViewBag.SelectedValue = GetItem.Id;


                if (GetItem.Name == "ParametarType")
                {
                    ViewBag.AddParametarType = GetItem.Id;
                    ViewBag.IsPersonal = "true";
                }

                if (ViewBag.SelectedValue > 0)
                {
                    ViewBag.model = GetItem.Name;
                }


            }
            ViewBag.IconsList = GetIcons();
        }


        private void GenerateParametarType(ModelForAdmin model, ItemName GetItem)
        {

            if (model.ValuesId != null && model.ValuesId[0] != "System.String[]")
            {
                GetItem.GenerateItems(model.ValuesId[1]);
             
                ViewBag.SelectedValue = GetItem.Id;
                
                if (GetItem.Name == "ParametarType")
                {
                    ViewBag.AddParametarType = GetItem.Id;
                    ViewBag.IsPersonal = "true";
                }
            }

        }


        private ModelForAdmin PupulateModels(ModelForAdmin model)
        {

            List<string> CmbControles = new List<string>() { "Models", "ParametarType", "GuiSectionType" };

            ModelForAdmin mda = AddCombo(CmbControles, model);

           
            return mda;
        }

        private ModelForAdmin AddCombo(List<string> CmbControles, ModelForAdmin model)
        {
            ModelForAdmin mdaCombo = new ModelForAdmin();


            foreach (string Name in CmbControles)
            {
           
                mdaCombo.DropdownsList.Add(GenerateSelectList(Name,model));
            }
            return mdaCombo;
        }


        private SelectList GenerateSelectList(string Name, ModelForAdmin model)
        {
            //Create a list of select list items - this will be returned as select list
            List<SelectListItem> newList = new List<SelectListItem>();
            int selectedindex = 0;
            foreach (KeyValuePair<int, string> entry in dalmethod.ReturnModels(Name))
            {
                bool selectedvalue = false;
                if (model.ValuesId != null)
                {
                    foreach (String value in model.ValuesId)
                    {
                        ItemName getvalue = new ItemName();
                        
                        getvalue.GenerateItems(value);
                        if (getvalue.Name == Name)
                        {
                            selectedvalue = true;
                            selectedindex = getvalue.Id;
                        }
              
                    }
                }

                SelectListItem selListItem = new SelectListItem();

                 selListItem.Text = entry.Value;
                 selListItem  . Value = entry.Key.ToString() + "_" + Name;
                 if (selectedindex == entry.Key)
                 {
                     selListItem.Selected = true;
                 }
                 else  selListItem.Selected = false;
                

                //Add select list item to list of selectlistitems
                newList.Add(selListItem);

            }

            //Return the list of selectlistitems as a selectlist
            return new SelectList(newList, "Value", "Text", null);

        }


        private List<string> GetIcons()
        {
            List<string> listaIcon = new List<string>();
            string line;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Content/Bootstrap 3 Glyphicons.txt")))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    listaIcon.Add(line); // Add to list.

                }

            }

            return listaIcon;

        }



        [HttpPost]
        public ActionResult ClientTrue(ModelForAdmin models)
        {
            TempData["message"] = "true";
            models.IsPersonal = "true";
            return RedirectToAction("AddParametar", models);
        }


        [HttpPost]
        public ActionResult ClientFalse(ModelForAdmin models)
        {
            TempData["message"] = "false";
            models.IsPersonal = "false";
            return RedirectToAction("AddParametar", models);
        }



        [HttpPost]
        public ActionResult AddParametarSwitch(ModelForAdmin models)
        {

            models.Parametar.AtributeList.Clear();
            if (models.Parametar.Name != null && models.ValuesId != null && models.Parametar.DescriptionLabel != null)
            {
                if (models.ValuesId[0] != "" && models.ValuesId[1] != "" && models.ValuesId[2] != "")
                {
                    ItemName getitem = new ItemName();
                    models.Parametar.BetaWeight = 1;
                    models.Parametar.ClientData = false;
                    models.Parametar.ParameterType = getitem.GenerateIndex(models.ValuesId[0]);
                    models.Parametar.ModelId = getitem.GenerateIndex(models.ValuesId[1]);
                    models.Parametar.CategoryId = getitem.GenerateIndex(models.ValuesId[2]);


                    AddParametar(models);

                    models.Parametar.AtributeList.Clear();
                    models.Parametar.Name = null;
                    models.Parametar.AtributeNameItems = null;
                    models.Parametar.BetaItems = null;
                    models.Parametar.NoInterval = 0;
                    models.ValuesId = null;
                }
            }
        
            return RedirectToAction ("AddParametar",models);
        }

        private void AddParametar(ModelForAdmin models)
        {

            bool isok = false;
            switch (models.Parametar.ParameterType)
            {
                case 3:
                    if (models.Parametar.Beta != null && models.Parametar.BetaWeight != null && models.Parametar.UperBound != null && models.Parametar.LowerBound != null)
                    {
                        isok = dalmethod.AddParametarContinuous(models.Parametar);
                    }
                    break;

                case 2:
                    if (models.Parametar.Beta != null && models.Parametar.BetaWeight != null && models.Parametar.UperBound != null && models.Parametar.LowerBound != null)
                    {
                        var modelparam = AddAtributesToList(models.Parametar);
                        isok = dalmethod.AddParametarDiscretizedContinuous(modelparam);
                        break;
                    }
                    break;

                case 1:
                    if (models.Parametar.Beta != null && models.Parametar.BetaWeight != null && models.Parametar.AtributeNameItems != null)
                    {
                        var modelparam = AddAtributesToList(models.Parametar);
                        isok = dalmethod.AddParametarDiscrete(modelparam);
                        break;
                    }
                    break;
            }
        }


        private ModelParam AddAtributesToList(ModelParam parametar)
        {

            for (int i = 0; i <= parametar.NoInterval -1; i++)
            {
                BetaAtribute atribute = new BetaAtribute();

                atribute.Beta = parametar.BetaItems[i];
                atribute.BetaWeight = parametar.BetaWeightItems[i];
                if (parametar.ParameterType == 3 || parametar.ParameterType == 2)
                {
                    atribute.LowerBound = parametar.LowerBoundItems[i];
                    atribute.UperBound = parametar.UperBoundItems[i];
                }
                if (parametar.ParameterType == 1 )
                {
                    atribute.AtributeName = parametar.AtributeNameItems[i];
                }
                atribute.index = i;

                parametar.AtributeList.Add(atribute);

            }

            /// Delete empty atribute rows 
            for (int i = 0; i < parametar.AtributeList.Count; i++ )
            {
                if (parametar.AtributeList[i].Beta == 0 && parametar.AtributeList[i].BetaWeight == 0 && parametar.AtributeList[i].UperBound == 0 && parametar.AtributeList[i].LowerBound == 0)
                {
                    parametar.AtributeList[i] = null;
                }
            }

                return parametar;
        }

	}
}