﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Azbok.Models;
using Azbok.DAL;
using System.IO;

namespace Azbok.Controllers
{
    public class EditParametarController : Controller
    {
        //
        // GET: /Admin/

        DAL.DalMethod dalmethod = new DalMethod();
        string[] Viewid = new string[4];

        public ActionResult EditParametar(ModelForAdmin model, bool? IsPersonal2)
        {
            ItemName GetItem = new ItemName();

            ViewBag.IsPersonal = TempData["message"];
            GenerateViewBag(model, GetItem);



            ModelForAdmin Model = new ModelForAdmin();

            model.ModelListParamModel.ObjectItems2.Clear();
            Model.DropdownsList.Clear();

            Model = PupulateModels();


            if (GetItem.Name == "Models")
            {
                Model.ActiveModel = GetItem.Id;
            }

            Model.ValueId = model.ValueId;
            return View(Model);
        }

        private void GenerateViewBag(ModelForAdmin model, ItemName GetItem)
        {
            if (model.ValueId != null)
            {

                GetItem.GenerateItems(model.ValueId);

                ViewBag.SelectedValue = GetItem.Id;


                if (GetItem.Name == "ParametarType")
                {
                    ViewBag.AddParametarType = GetItem.Id;
                    ViewBag.IsPersonal = "true";
                }

                if (ViewBag.SelectedValue > 0)
                {
                    ViewBag.model = GetItem.Name;
                }


            }
            ViewBag.IconsList = GetIcons();
        }

        private ModelForAdmin PupulateModels()
        {

            List<string> CmbControles = new List<string>() { "Models", "ParametarType" };

            ModelForAdmin mda = AddCombo(CmbControles);

            foreach (ModelParam m in PopulateParametars().ObjectItems2)
            {
                mda.ModelListParamModel.ObjectItems2.Add(m);
            }
            return mda;
        }

        private ModelForAdmin AddCombo(List<string> CmbControles)
        {
            ModelForAdmin mdaCombo = new ModelForAdmin();


            foreach (string Name in CmbControles)
            {
              
                mdaCombo.DropdownsList.Add(GenerateSelectList(Name));
            }
            return mdaCombo;
        }


        private SelectList GenerateSelectList(string Name)
        {
            //Create a list of select list items - this will be returned as select list
            List<SelectListItem> newList = new List<SelectListItem>();

            foreach (KeyValuePair<int, string> entry in dalmethod.ReturnModels(Name))
            {
                SelectListItem selListItem = new SelectListItem()
                {
                    Text = entry.Value,
                    Value = entry.Key.ToString() + "_" + Name,
                    Selected = false
                };

                //Add select list item to list of selectlistitems
                newList.Add(selListItem);

            }

            //Return the list of selectlistitems as a selectlist
            return new SelectList(newList, "Value", "Text", null);

        }


        public ModelParamList PopulateParametars()
        {
            DalMethod Method = new DalMethod();

            var ParametarsList = Method.ReadParametarField();
            ModelParamList paramlist = new ModelParamList();
            foreach (Azbok.Models.ModelParam p in ParametarsList)
            {

                ModelParam list2 = new ModelParam()
                {
                    Category = p.Category,
                    ControlTypeString = p.ControlTypeString,
                    DescriptionLabel = p.DescriptionLabel,
                    IsDefault = p.IsDefault,
                    Icon = p.Icon,
                    Id = p.Id,
                    Name = p.Name,
                    Placeholder = p.Placeholder,
                    OrderIndex = p.OrderIndex,
                    ClientData = p.ClientData,
                    ParameterType = p.ParameterType,
                    ModelId = p.ModelId,

                };

                paramlist.ObjectItems2.Add(list2);
            }
            return paramlist;

        }

        /// <summary>
        /// Upadate Order for Prametar items position 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fromPosition"></param>
        /// <param name="toPosition"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public ActionResult UpdateOrder(int id, int fromPosition, int toPosition, string direction)
        {
            DalMethod dal = new DalMethod();
            bool isupdated = dal.ChangeOrderPostion(id, fromPosition, toPosition, direction);

            return RedirectToAction("Admin");
        }

        private List<string> GetIcons()
        {
            List<string> listaIcon = new List<string>();
            string line;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Content/Bootstrap 3 Glyphicons.txt")))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    listaIcon.Add(line); // Add to list.

                }

            }

            return listaIcon;

        }






        /// <summary>
        /// Edit/Delete/Details Parametars 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(ModelForAdmin model)
        {


            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {

            return View();
        }

        [HttpPost]
        public ActionResult Details(int id)
        {

            return View();
        }


        [HttpPost]
        public ActionResult ClientTrue(ModelForAdmin models)
        {
            TempData["message"] = "true";
            models.IsPersonal = "true";
            return RedirectToAction("Admin", models);
        }


        [HttpPost]
        public ActionResult ClientFalse(ModelForAdmin models)
        {
            TempData["message"] = "false";
            models.IsPersonal = "false";
            return RedirectToAction("EditParametar", models);
        }

    }
}