﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azbok.Models
{
    public class BetaAtribute
    {
        public int index { get; set; }

        public decimal Beta { get; set; }
        public decimal BetaWeight { get; set; }
        public decimal LowerBound { get; set; }
        public decimal UperBound { get; set; }
        public string AtributeName { get; set; }
        
    }
}