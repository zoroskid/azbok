﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Azbok.Models
{
    public class ModelForAdmin
    {

        /// <summary>
        /// Model for Parmetars
        /// </summary>
        private static ModelParamList _ModelListParamModel = new ModelParamList();
        public ModelParamList ModelListParamModel { get { return _ModelListParamModel; } }

        /// <summary>
        /// Model for DropDownList
        /// </summary>
        private static List<SelectList> _DropDownsList = new List<SelectList>();
        public List<SelectList> DropdownsList { get { return _DropDownsList; } }


        [ScaffoldColumn(false)]
        public string ValueId { get; set; }



        [ScaffoldColumn(false)]
        public string[] ValuesId { get; set; }

        public int ActiveModel { get; set; }

        public string IsPersonal { get; set; }


        public ModelParam Parametar { get; set; }

    }


}