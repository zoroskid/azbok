﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azbok.Models
{

    /// <summary>
    /// Choice for ComboBox (Mvc view)
    /// </summary>
    public class Choice
    {
        public int IdModel { get; set; }
        public string Text { get; set; }
    }
}