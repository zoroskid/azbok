﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web;
using System.Web.Mvc;

namespace Azbok.Models
{
    public class ModelParam
    {

        private static List<BetaAtribute> _AtributeList = new List<BetaAtribute>();

        public ControlsType.Control ControlType { get; set; }
        public string ControlTypeString { get; set; }
        public string Category { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public long Id { get; set; }
        public string Value { get; set; }
        public string Placeholder { get; set; }
        public string DescriptionLabel { get; set; }
        public int OrderIndex { get; set; }
        public bool? IsDefault { get; set; }
        /// <summary>
        /// Icon of Bootstrap Glaficons
        /// </summary>
        public string Icon { get; set; }
        public long ModelId { get; set; }
        public bool? ClientData { get; set; }
        public long ParameterType { get; set; }

        /// <summary>
        /// Definition for Continus parametar int
        /// </summary>
        public decimal Beta { get; set; }
        public decimal BetaWeight { get; set; }
        public decimal LowerBound { get; set; }
        public decimal UperBound { get; set; }
        
        
        public int NoInterval { get; set; }


        /// <summary>
        /// Definition for discretizedContinus parametar list[]
        /// </summary>
        /// 
        public decimal[] BetaItems { get; set; }
        public decimal[] BetaWeightItems { get; set; }
        public decimal[] LowerBoundItems { get; set; }
        public decimal[] UperBoundItems { get; set; }
        public string[] AtributeNameItems { get; set; }
        public List<BetaAtribute> AtributeList { get { return _AtributeList; } }

        public int indexOfCmb { get; set; }

        private static List<SelectList> _DropDownsList = new List<SelectList>();
        public List<SelectList> DropdownsList { get { return _DropDownsList; } }
       


    }
}