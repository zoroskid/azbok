﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Azbok.Models
{
    public class ModelParamList
    {

        [ScaffoldColumn(false)]
        public string[] ValuesId { get; set; }


        public List<ModelParam> ObjectItems2 { get; set; }
        public List<AnswerDisplayItem> Answers { get; set; }
        public List<AnswerDisplayItemCmb> AnswersCmb { get; set; }


        public string Id { get; set; }

        public ModelParamList()
        {
            Answers = new List<AnswerDisplayItem>();
            AnswersCmb = new List<AnswerDisplayItemCmb>();
            ObjectItems2 = new List<ModelParam>();

        }

        public class AnswerDisplayItem
        {
                  
            public string Value { get; set; }
            public string Name { get; set; }
            public int Id { get; set; }

        }

        public class AnswerDisplayItemCmb
        {
            public string Value { get; set; }
            public string Name { get; set; }
            public int Id { get; set; }

        }
    }
}