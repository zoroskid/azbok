﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azbok.Models
{
    public class UserData
    {      


       public string ime { get; set; }
       public string prezime { get; set; }
       public string imeOca { get; set; }
       public string drzavljanstvo { get; set; }
       public string jmbg { get; set; }
       public string brojLicneKarte { get; set; }
       public string rodjenjeDan { get; set; }
       public string rodjenjeMesec { get; set; }
       public string rodjenjeGodina { get; set; }
       public string rodjenjeDrzava { get; set; }
       public string rodjenjeOpstina { get; set; }
       public string rodjenjeGrad { get; set; }
       public string pol { get; set; }
       public string brak { get; set; }
       public string stanovanjeGrad { get; set; }
       public string stanovanjeOpstina { get; set; }
       public string stanovanjeUlica { get; set; }
       public string stanovanjeDan { get; set; }
       public string stanovanjeMesec { get; set; }
       public string stanovanjeGodina { get; set; }
       public string zakupninaIznos { get; set; }
       public string mobilni { get; set; }
       public string fiksni { get; set; }
       public string email { get; set; }
       public string kompanijaPozicija { get; set; }
       public string kompanijaWeb { get; set; }
       public string mesecnaPrimanja { get; set; }
       public string Pib { get; set; }
       public string iznos { get; set; }
       public string primanjaValuta { get; set; }
       public string godineStaza { get; set; }
       public string zaposlenjeStatus { get; set; }
       public string obustavljeniIznos { get; set; }
       public string obustavljeniIznosValuta { get; set; }
       public string vrstaSvojine { get; set; }
       public string stecenoZvanje { get; set; }
       public string trazeniIznos { get; set; }
       public string kreditValuta { get; set; }
       public string rokOtplateMeseci { get; set; }
       public string ProcenatUcesca { get; set; }
       public string kompanijaIme { get; set; }
       public string tip { get; set; }



    }
}