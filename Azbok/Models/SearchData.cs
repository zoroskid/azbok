﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azbok.Models
{
    public class SearchData
    {

        public string SearchName  { get; set; }
        public string SearchLastName { get; set; }        
        public int SearchCode { get; set; }

        public long SearchJmbg { get; set; }

        public string SearchType { get; set; }


       
    }
}