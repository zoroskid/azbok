﻿using System;

namespace Azbok.Models
{
    public class ModelCreditRequest
    {
        //from CreditRequest table
        public long ID { get; set; }
        public int SegmentCreditTypeID { get; set; }
        public int CreditID { get; set; }
        public int UserProfileID { get; set; }
        public long RequestStatusID { get; set; }
        public int CalculationStatusID { get; set; }
        public string Code { get; set; }
        public decimal? PD { get; set; }
        public decimal Score { get; set; }
        public int Rating { get; set; }
        public bool Edited { get; set; }
        public string Attachment { get; set; }
        public DateTime? RequestDatetime { get; set; }
        public DateTime ResponseDatetime { get; set; }
        public DateTime LastOverrideDatetime { get; set; }

        //from joined tables
        public string CreditRequestOwnerName { get; set; }
        public string CreditRequestOwnerLastName { get; set; }
        public string CreditRequestOwnerID { get; set; }//JMBG ili PIB
        public string CriteriaType { get; set; } //PD, Score or Rating
        public decimal? CriteriaValue { get; set; }
        public string SegmentName { get; set; }
        public string CreditType { get; set; }
        public string RequestStatusName { get; set; }
        //formated data
        public string RequestDatetimeToString { get; set; }
    }
}