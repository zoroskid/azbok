﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Azbok.Models
{
    public class ItemName
    {
        public string Name { get; set; }

        public int Id { get; set; }


        public void GenerateItems(string Item)
        {
            if (Item != ""  && Item != "System.String[]")
            {
                StringBuilder sbName = new StringBuilder();
                StringBuilder sbIndex = new StringBuilder();


                try
                {
                    foreach (char c in Item)
                    {

                        if (Char.IsNumber(c)) sbIndex.Append(c);

                        else if (c != '_') sbName.Append(c);
                    }


                    Name = sbName.ToString();

                    if (Name.EndsWith(" "))
                        Name = Name.Substring(0, Name.Length - 1);

                    Id = int.Parse(sbIndex.ToString());
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        public int GenerateIndex(string Item)
        {
            if (Item != "")
            {
             
                StringBuilder sbIndex = new StringBuilder();


                try
                {
                    foreach (char c in Item)
                    {

                        if (Char.IsNumber(c)) sbIndex.Append(c);

                    }


                    return  int.Parse(sbIndex.ToString());
                }
                catch (Exception)
                {
                    return 0; ;
                    throw;
                   
                }
            }

            return 0;
        }

     
    }
}