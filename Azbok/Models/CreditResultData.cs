﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azbok.Models
{
    public class CreditResultData
    {

        public int Rata { get; set; }

        public double Isplata { get; set; }

        public double Anuitet { get; set; }

        public double Otplata { get; set; }

        public double Uplata { get; set; }

        public double Ucesce { get; set; }

        public double Stanje { get; set; }

        public static List<CreditResultData> GetData()
        { 
            return new List<CreditResultData>()
            {
                new CreditResultData() { Rata=0, Isplata=50000,Anuitet=0.00,Otplata=0.00,Uplata=0.00,Ucesce=1.000,Stanje= 4.00000},
                new CreditResultData() { Rata=1, Isplata=0.000,Anuitet=359.15,Otplata=312.48,Uplata=46.67,Ucesce=0.00,Stanje= 3.687 },
                new CreditResultData() { Rata=2, Isplata=0.000,Anuitet=359.15,Otplata=312.48,Uplata=43.47,Ucesce=0.000,Stanje=3.687 },
                new CreditResultData() { Rata=3, Isplata=0.000,Anuitet=359.15,Otplata=0.00,Uplata=0.00,Ucesce=1.000,Stanje= 4.00000 },
                new CreditResultData() { Rata=4, Isplata=0.000,Anuitet=359.15,Otplata=0.00,Uplata=0.00,Ucesce=1.000,Stanje= 4.00000 },
                new CreditResultData() { Rata=5, Isplata=0.000,Anuitet=359.15,Otplata=0.00,Uplata=0.00,Ucesce=1.000,Stanje= 4.00000 },
                new CreditResultData() { Rata=6, Isplata=0.000,Anuitet=359.15,Otplata=0.00,Uplata=0.00,Ucesce=1.000,Stanje= 4.00000 },
                new CreditResultData() { Rata=7, Isplata=0.000,Anuitet=359.15,Otplata=0.00,Uplata=0.00,Ucesce=1.000,Stanje= 4.00000 },
                new CreditResultData() { Rata=8, Isplata=0.000,Anuitet=359.15,Otplata=0.00,Uplata=0.00,Ucesce=1.000,Stanje= 4.00000 },
                new CreditResultData() { Rata=9, Isplata=0.000,Anuitet=359.15,Otplata=0.00,Uplata=0.00,Ucesce=1.000,Stanje= 4.00000 }
            };       
        }
    }
}