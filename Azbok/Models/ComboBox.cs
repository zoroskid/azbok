﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Azbok.Models
{
    public class ComboBox
    {
        public int SelectedId { get; set; }
        public string ComboboxName { get; set; }

        private static List<SelectListItem> _Choices = new List<SelectListItem>();
       

        public List<SelectListItem> Choices { get { return _Choices; } }
             
    }
}