﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Azbok.Models;

namespace Azbok.Models
{
    public class UsersApp
    {
        private readonly List<SearchData> _Searchdata = new List<SearchData>();

        public List<SearchData> usersall { get { return _Searchdata; } }
    }
}