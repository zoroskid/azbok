//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Azbok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CriteriaBound
    {
        public CriteriaBound()
        {
            this.wSegmentCreditTypes = new HashSet<wSegmentCreditType>();
        }
    
        public long ID { get; set; }
        public Nullable<long> CriteriaID { get; set; }
        public Nullable<long> RatingScaleID { get; set; }
        public Nullable<decimal> BoundForApproval { get; set; }
        public Nullable<decimal> BoundForRejection { get; set; }
        public Nullable<bool> ApprovalSortingAsc { get; set; }
    
        public virtual wCriteria wCriteria { get; set; }
        public virtual wRatingScale wRatingScale { get; set; }
        public virtual ICollection<wSegmentCreditType> wSegmentCreditTypes { get; set; }
    }
}
