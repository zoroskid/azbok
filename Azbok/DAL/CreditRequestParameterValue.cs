//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Azbok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CreditRequestParameterValue
    {
        public long ID { get; set; }
        public long CreditRequestID { get; set; }
        public long ParameterID { get; set; }
        public Nullable<decimal> Value { get; set; }
        public Nullable<decimal> RecalculatedValue { get; set; }
        public string ClientDataValue { get; set; }
    
        public virtual CreditRequest CreditRequest { get; set; }
    }
}
