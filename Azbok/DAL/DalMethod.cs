﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Azbok.Controllers;
using Azbok.DAL;
using Azbok.Models;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.Ajax.Utilities;
using Org.BouncyCastle.Ocsp;

namespace Azbok.DAL
{
    public class DalMethod
    {
        public List<CreditRequest> CheckFlag(CreditRequest c)
        {

            AccurateEntities ratingSystemEf = new AccurateEntities();

            List<CreditRequest> lista = new List<CreditRequest>();

            try
            {
                var lista2 = from st in ratingSystemEf.CreditRequests
                             where st.CalculationStatusID == 0 && st.wSegmentCreditType == c.wSegmentCreditType
                             select st;

                lista = lista2.ToList();
            }
            catch (Exception e)
            {

            }

            if (lista.Count > 0)
            {
                //TO DO  
            }

            return lista;
        }


        public IEnumerable<ModelParam> ReadParametarField()
        {
            AccurateEntities ratingSystemEf = new AccurateEntities();
            ParamAndParamField();
            try
            {
                var lista2 = (from st in ratingSystemEf.ParameterFields
                              join param in ratingSystemEf.Parameters on st.ParameterID equals param.ID
                              orderby st.PositionID
                              select new ModelParam
                              {
                                  ControlTypeString = st.ControlType,
                                  Id = st.ParameterID,
                                  OrderIndex = st.PositionID,
                                  ModelId = param.ModelID,
                                  Name = param.Name,
                                  Placeholder = st.Placeholder,
                                  DescriptionLabel = st.DescriptionLabel,
                                  Icon = st.Icon,
                                  Category = st.Category,
                                  ParameterType = param.ParameterTypeID

                              }).ToList();


                return lista2;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public void ParamAndParamField()
        {

            AccurateEntities ratingSystemEf = new AccurateEntities();

            try
            {
                var lista2 = (from st in ratingSystemEf.ParameterFields
                              join param in ratingSystemEf.Parameters on st.ID equals param.ID
                              orderby st.PositionID
                              select new
                              {
                                  ControlType = st.ControlType,
                                  ID = st.ID,
                                  PositionId = st.PositionID,
                                  Model = param.ModelID,
                                  Name = param.Name,
                                  Placeholder = st.Placeholder,
                                  DescriptionLabel = st.DescriptionLabel,
                                  OrderIndex = st.PositionID,
                                  Icon = st.Icon,
                                  Category = st.Category,
                                  ParameterType = param.ParameterTypeID

                              }).ToList();



            }
            catch (Exception e)
            {

            }

        }


        public void AddParametarField(ModelParam m)
        {
            using (AccurateEntities ratingSystemEf = new AccurateEntities())
            {
                ParameterField parametar = new ParameterField()
                {
                    Category = m.Category.ToString(),
                    ControlType = m.ControlType.ToString(),
                    DescriptionLabel = m.DescriptionLabel,
                    Icon = m.Icon,
                    ID = m.Id,
                    Name = m.Name,
                    IsDefault = m.IsDefault,
                    Placeholder = m.Placeholder,
                    PositionID = m.OrderIndex
                };

                ratingSystemEf.ParameterFields.Add(parametar);

                ratingSystemEf.SaveChanges();


            }
        }

        public bool ChangeOrderPostion(int id, int fromPosition, int toPosition, string direction)
        {
            try
            {

                using (AccurateEntities ratingSystemEf2 = new AccurateEntities())
                {
                    var _parametarToSwap = (from s in ratingSystemEf2.ParameterFields
                                            where s.PositionID == fromPosition
                                            select s).FirstOrDefault();




                    var _parametarToSwapWith = (from s in ratingSystemEf2.ParameterFields
                                                where s.PositionID == toPosition && s.ID != id
                                                select s).FirstOrDefault();


                    _parametarToSwap.PositionID = toPosition;
                    _parametarToSwapWith.PositionID = fromPosition;
                    int num = ratingSystemEf2.SaveChanges();
                    int num2 = ratingSystemEf2.SaveChanges();
                }



                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public Dictionary<int, string> ReturnModels(string type)
        {

            Dictionary<int, string> _Models = new Dictionary<int, string>();
            try
            {

                using (AccurateEntities ratingSystemEf2 = new AccurateEntities())
                {
                    if (type == "Models")
                    {
                        var _ModelName = (from s in ratingSystemEf2.Models
                                          select s.Name).ToList();

                        var _ModelId = (from s in ratingSystemEf2.Models
                                        select s.ID).ToList();

                        for (int i = 0; i <= _ModelId.Count - 1; i++)
                        {
                            _Models.Add((int)_ModelId[i], _ModelName[i]);
                        }
                    }

                    if (type == "ParametarType")
                    {

                        var _ModelName = (from s in ratingSystemEf2.wParameterTypes
                                          select s.Description).ToList();

                        var _ModelId = (from s in ratingSystemEf2.wParameterTypes
                                        select s.ID).ToList();

                        for (int i = 0; i <= _ModelId.Count - 1; i++)
                        {
                            _Models.Add((int)_ModelId[i], _ModelName[i]);
                        }

                    }

                    if (type == "GuiSectionType")
                    {

                        var _ModelName = (from s in ratingSystemEf2.wGuiSectionTypes
                                          select s.Description).ToList();

                        var _ModelId = (from s in ratingSystemEf2.wGuiSectionTypes
                                        select s.ID).ToList();

                        for (int i = 0; i <= _ModelId.Count - 1; i++)
                        {
                            _Models.Add((int)_ModelId[i], _ModelName[i]);
                        }

                    }

                }
                return _Models;

            }
            catch (Exception e)
            {
                return null;
            }

        }


        private string GetParametarTypes(int SelectedIndex)
        {

            using (var ratingSystemEf2 = new AccurateEntities())
            {
                var _ModelName = (from s in ratingSystemEf2.wGuiSectionTypes
                                  where s.ID == SelectedIndex
                                  select s.Description).First();

                return _ModelName;
            }

        }


        public bool AddParametarContinuous(ModelParam parametar)
        {

            parametar.Category = GetParametarTypes(parametar.CategoryId);

            try
            {
                using (AccurateEntities ratingSystemEf2 = new AccurateEntities())
                {

                    Parameter parametartable = new Parameter();
                    parametartable.ModelID = parametar.ModelId;
                    parametartable.Enabled = true;
                    parametartable.GuiSectionTypeID = parametar.CategoryId;
                    parametartable.Name = parametar.Name;
                    parametartable.ParameterTypeID = parametar.ParameterType;
                    ratingSystemEf2.Parameters.Add(parametartable);
                    ratingSystemEf2.SaveChanges();

                    /// get parametar id
                    int parametarid = getparametarid(parametartable);

                    ParameterField paramaetarfield = new ParameterField();
                    paramaetarfield.ParameterID = parametarid;
                    paramaetarfield.Category = parametar.Category;
                    paramaetarfield.ControlType = "TextBox";
                    paramaetarfield.Name = parametar.Name;
                    paramaetarfield.DescriptionLabel = parametar.DescriptionLabel;
                    paramaetarfield.Icon = "glyphicon glyphicon-signal";
                    paramaetarfield.IsDefault = true;
                    ///TO DO
                    paramaetarfield.PositionID = GenLastPositionIndex(parametar);
                    ///
                    paramaetarfield.Placeholder = parametar.Placeholder;


                    ratingSystemEf2.ParameterFields.Add(paramaetarfield);




                    ParameterAttribute attribute = new ParameterAttribute();
                    attribute.AttributeID = 1;
                    attribute.Beta = parametar.Beta;
                    attribute.BetaWeight = parametar.BetaWeight;
                    attribute.LowerBound = parametar.LowerBound;
                    attribute.Name = parametar.Name;
                    //TO DO
                    attribute.ParameterID = parametarid;
                    //
                    attribute.UpperBound = parametar.UperBound;


                    ratingSystemEf2.ParameterAttributes.Add(attribute);

                    ratingSystemEf2.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
                throw;
            }

            return true;
        }

        public bool AddParametarDiscretizedContinuous(ModelParam parametar)
        {

            parametar.Category = GetParametarTypes(parametar.CategoryId);

            try
            {
                using (AccurateEntities ratingSystemDiscCont = new AccurateEntities())
                {

                    Parameter parametartable = new Parameter();
                    parametartable.ModelID = parametar.ModelId;
                    parametartable.Enabled = true;
                    parametartable.GuiSectionTypeID = parametar.CategoryId;
                    parametartable.Name = parametar.Name;
                    parametartable.ParameterTypeID = parametar.ParameterType;
                    ratingSystemDiscCont.Parameters.Add(parametartable);
                    ratingSystemDiscCont.SaveChanges();

                    /// get parametar id
                    int parametarid = getparametarid(parametartable);

                    ParameterField paramaetarfield = new ParameterField();
                    paramaetarfield.ParameterID = parametarid;
                    paramaetarfield.Category = parametar.Category;
                    paramaetarfield.ControlType = "TextBox";
                    paramaetarfield.DescriptionLabel = parametar.DescriptionLabel;
                    paramaetarfield.Icon = "glyphicon glyphicon-signal";
                    paramaetarfield.IsDefault = true;
                    paramaetarfield.Name = parametar.Name;
                    paramaetarfield.PositionID = GenLastPositionIndex(parametar);
                    paramaetarfield.Placeholder = parametar.Placeholder;


                    ratingSystemDiscCont.ParameterFields.Add(paramaetarfield);



                    for (int i = 0; i <= parametar.AtributeList.Count - 1; i++)
                    {
                        if (parametar.AtributeList[i] != null)
                        {
                            ParameterAttribute attribute = new ParameterAttribute();
                            attribute.AttributeID = i + 1;
                            attribute.Beta = parametar.AtributeList[i].Beta;
                            attribute.BetaWeight = parametar.AtributeList[i].BetaWeight;
                            attribute.LowerBound = parametar.AtributeList[i].LowerBound;
                            attribute.Name = parametar.Name;
                            attribute.ParameterID = parametarid;
                            attribute.UpperBound = parametar.AtributeList[i].UperBound;
                            ratingSystemDiscCont.ParameterAttributes.Add(attribute);
                        }
                    }
                    ratingSystemDiscCont.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
                throw;
            }

            return true;
        }

        public bool AddParametarDiscrete(ModelParam parametar)
        {

            parametar.Category = GetParametarTypes(parametar.CategoryId);

            try
            {
                using (AccurateEntities ratingSystemDisc = new AccurateEntities())
                {

                    Parameter parametartable = new Parameter();
                    parametartable.ModelID = parametar.ModelId;
                    parametartable.Enabled = true;
                    parametartable.GuiSectionTypeID = parametar.CategoryId;
                    parametartable.Name = parametar.Name;
                    parametartable.ParameterTypeID = parametar.ParameterType;

                    ratingSystemDisc.Parameters.Add(parametartable);
                    ratingSystemDisc.SaveChanges();

                    /// get parametar id
                    int parametarid = getparametarid(parametartable);

                    ParameterField paramaetarfield = new ParameterField();
                    paramaetarfield.ParameterID = parametarid;
                    paramaetarfield.Category = parametar.Category;
                    paramaetarfield.ControlType = "ComboBox";
                    paramaetarfield.DescriptionLabel = parametar.DescriptionLabel;
                    paramaetarfield.Icon = "glyphicon glyphicon-signal";
                    paramaetarfield.IsDefault = true;
                    paramaetarfield.Name = parametar.Name;
                    paramaetarfield.PositionID = GenLastPositionIndex(parametar);

                    ratingSystemDisc.ParameterFields.Add(paramaetarfield);

                    for (int i = 0; i <= parametar.AtributeList.Count - 1; i++)
                    {
                        if (parametar.AtributeList[i] != null)
                        {
                            ParameterAttribute attribute = new ParameterAttribute();
                            attribute.AttributeID = i + 1;
                            attribute.Beta = parametar.AtributeList[i].Beta;
                            attribute.BetaWeight = parametar.AtributeList[i].BetaWeight;
                            attribute.Name = parametar.AtributeList[i].AtributeName;
                            attribute.ParameterID = parametarid;
                            ratingSystemDisc.ParameterAttributes.Add(attribute);
                        }
                    }
                    ratingSystemDisc.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
                throw;
            }

            return true;
        }


        private int getparametarid(Parameter p)
        {
            using (AccurateEntities ratingSystemEf = new AccurateEntities())
            {

                var ParamId = (from st in ratingSystemEf.Parameters
                               where st.Name == p.Name
                               select st.ID).ToList().Last();

                return (int)ParamId;
            }
        }

        private int GenLastPositionIndex(ModelParam param)
        {
            int last;
            try
            {
                using (AccurateEntities ratingSystemEf = new AccurateEntities())
                {

                    var LastIndex = (from st in ratingSystemEf.ParameterFields
                                     where st.Category == param.Category
                                     orderby st.PositionID
                                     select st.PositionID).ToList().Last();


                    return LastIndex + 1;

                }
            }
            catch (Exception)
            {
                throw;
            }



        }


        public Dictionary<int, string> GenerateComboBox(long Id)
        {

            Dictionary<int, string> _CmbItems = new Dictionary<int, string>();
            try
            {

                using (AccurateEntities ratingSystemEf2 = new AccurateEntities())
                {
                    var lista2 = (from st in ratingSystemEf2.ParameterAttributes
                                  join param in ratingSystemEf2.Parameters on st.ParameterID equals param.ID
                                  where st.ParameterID == Id
                                  select st).ToList();

                    for (int i = 0; i <= lista2.Count - 1; i++)
                    {
                        _CmbItems.Add((int)lista2[i].AttributeID, lista2[i].Name);
                    }
                }
                return _CmbItems;
            }

            catch (Exception e)
            {
                return null;
            }

        }

        //Srdjan
        //todo mm da li je ok iz DAL-a vratiti napunjen model?
        /// <summary>
        /// return list of ModelCreditRequest, for given calculationStatus
        /// </summary>
        /// <param name="calculationStatus"></param>
        /// <returns></returns>
        public IList<ModelCreditRequest> GetCreditRequests(int? requestStatus)
        {
            var mcrList = new List<ModelCreditRequest>();
            using (var dbContext = new AccurateEntities())
            {
                //var r1 = (from cr in dbContext.CreditRequests
                //          join wsct in dbContext.wSegmentCreditTypes on cr.SegmentCreditTypeID equals wsct.ID
                //          join mod in dbContext.Models on wsct.ModelID equals mod.ID
                //          join param in dbContext.Parameters on mod.ID equals param.ModelID
                //          join crpv in dbContext.CreditRequestParameterValues on param.ID equals crpv.ParameterID
                //          where cr.CalculationStatusID == calculationStatus &&
                //                param.ParameterTypeID == parameterType &&
                //                param.Name.ToLower() == name
                //          select new { cr, wsct, mod, param, crpv }).ToList();

                ////var result = (dbContext.CreditRequests .Where(cr => cr.RequestStatusID == calculationStatus).

                //foreach (var creditRequest in r1)
                //{
                //    var aab = r1.Where(a => a.param.ParameterTypeID == 4).ToList();

                //}
                // ReSharper disable once ImplicitlyCapturedClosure
                List<CreditRequest> creditRequestList =
                    dbContext.CreditRequests.Where(cr => cr.RequestStatusID == requestStatus).ToList();
                foreach (var creditRequest in creditRequestList)
                {
                    ModelCreditRequest mcr = new ModelCreditRequest();
                    //ClientName
                    mcr.CreditRequestOwnerName = (from table1 in creditRequest.wSegmentCreditType.Model.Parameters
                                                  join crpv in dbContext.CreditRequestParameterValues on table1.ID equals crpv.ParameterID
                                                  where table1.ParameterTypeID == 5 && table1.Name.ToLower() == "clientname"
                                                  select crpv.ClientDataValue).FirstOrDefault();
                    //ClientLastName
                    mcr.CreditRequestOwnerLastName = (from table1 in creditRequest.wSegmentCreditType.Model.Parameters
                                                      join crpv in dbContext.CreditRequestParameterValues on table1.ID equals crpv.ParameterID
                                                      where table1.ParameterTypeID == 5 && table1.Name.ToLower() == "clientlastname"
                                                      select crpv.ClientDataValue).FirstOrDefault();
                    //JMBG/PIB
                    mcr.CreditRequestOwnerID = (from table1 in creditRequest.wSegmentCreditType.Model.Parameters
                                                join crpv in dbContext.CreditRequestParameterValues on table1.ID equals crpv.ParameterID
                                                where
                                                    table1.ParameterTypeID == 5 &&
                                                    (table1.Name.ToLower() == "jmbg" || table1.Name.ToLower() == "pib")
                                                select crpv.ClientDataValue).FirstOrDefault();
                    //CreditRequestID
                    mcr.ID = creditRequest.ID;
                    //Segment
                    mcr.SegmentName = creditRequest.wSegmentCreditType.wSegment.Name;
                    //CreditType
                    mcr.CreditType = creditRequest.wSegmentCreditType.wCreditType.Name;
                    //CriteriaType (PD/Score/Rating)
                    mcr.CriteriaType = creditRequest.wSegmentCreditType.CriteriaBound.wCriteria.Name;
                    //PD/Score/Rating value
                    switch (creditRequest.wSegmentCreditType.CriteriaBound.wCriteria.ID)
                    {
                        case 1:
                            mcr.CriteriaValue = creditRequest.PD;
                            break;
                        case 2:
                            mcr.CriteriaValue = creditRequest.Score;
                            break;
                        case 3:
                            mcr.CriteriaValue = creditRequest.Rating;
                            break;
                        default:
                            throw new Exception();
                    }
                    mcr.RequestDatetime = creditRequest.RequestDatetime;
                    mcr.RequestStatusID = creditRequest.RequestStatusID;
                    mcrList.Add(mcr);
                }
            }
            return mcrList;
        }

        public void ChangeCreditRequestStatus(long creditRequestID, int creditRequestStatus)
        {
            using (var dbContext = new AccurateEntities())
            {
                dbContext.CreditRequests.Where(cr => cr.ID == creditRequestID).FirstOrDefault().RequestStatusID = creditRequestStatus;
                dbContext.SaveChanges();
            }
        }
    }
}
