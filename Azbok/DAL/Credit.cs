//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Azbok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Credit
    {
        public Credit()
        {
            this.CreditRequests = new HashSet<CreditRequest>();
        }
    
        public long ID { get; set; }
        public Nullable<decimal> Principal { get; set; }
        public Nullable<int> RepaymentPeriod { get; set; }
        public Nullable<decimal> AnnualInterestRate { get; set; }
        public Nullable<int> RepaymentFrequency { get; set; }
        public string CurrencyPay { get; set; }
        public string CurrencyRef { get; set; }
    
        public virtual ICollection<CreditRequest> CreditRequests { get; set; }
    }
}
