//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Azbok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserProfile
    {
        public UserProfile()
        {
            this.CreditRequests = new HashSet<CreditRequest>();
        }
    
        public long ID { get; set; }
        public long UserID { get; set; }
        public long ProfileID { get; set; }
    
        public virtual ICollection<CreditRequest> CreditRequests { get; set; }
        public virtual Profile Profile { get; set; }
        public virtual User User { get; set; }
    }
}
